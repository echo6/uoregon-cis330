#include <iostream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;

class Pizza{
private:
	//container data member for toppings.
	std::vector<string> topping;

public:

 	//a default constructor (no arguments) and Choose appropriate data member values
	Pizza(){
		string basic[] = {"chese", "marinara"};
		vector<string> v(basic, basic+sizeof(basic)/ sizeof(basic));
		topping = v;
		cout<<"pizaa basic topping: ";
		for (std::vector<string>::const_iterator i= topping.begin(); i !=topping.end();i++){
			std::cout<<*i<< ' ';
		}
	}
	//constructor which accepts a vector<string> of toppings as initial values
	Pizza(vector<string> topp){
		topping = topp;
		cout<<"pizaa optional topping: ";

	}
	~Pizza(){
		
	}

};

int main(){

	string toppings[] = {"pepperoni", "sausage", "ham", "chicken"};
	vector<string> v(toppings, toppings+sizeof(toppings)/sizeof(string));

	Pizza p1;
	Pizza p2(v);


	return 0;
}

/*
 

 */