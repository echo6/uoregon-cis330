#ifndef STUDENT_HPP_
#define STUDENT_HPP_

#include <fstream>
#include <string>

class Student{
public:

	Student();	//Default constructor
	Student();	//Copy constructor
	Student();	//Move constructor 
	Student(string firstName, string lastName, int age);	//A constructor, which initializes the data members with input arguments
	~Student();

	String getFirstName();
	String getLastName();
	int getAge();

	bool setFirstName(std::string firstName);
	bool setLastName(std::string lastName);
	bool setAge(int age);

private:
	std::string firstName;
	std::string lastName;
	int age;
}

#endif /* STUDENT_HPP_ */