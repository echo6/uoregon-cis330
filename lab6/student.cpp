#include <>

Student::Student(){
	cout<<"default constructor"<<this.getFirstName()<<this.getLastName()<<endl;

}

Student::Student(string firstName, string lastName, int age){
	cout<<"constructor with input values"<<endl;

}

Student::Student(){

}

Student::Student(){

}

Student::~Student(){
	cout<<"destructor"<<endl;
}

String Student::getFirstName(){
	return this.firstName;
}

String Student::getLastName(){
	return this.lastName;
}

int Student::getAge(){
	return this.age;
}

bool Student::setFirstName(std::string firstName){
	if(firstName.length() ==0){
		return false;
	}
	else{
		this.firstName = firstName;
		return true;
	}
}

bool Student::setLastName(std::string lastName){
	if(lastName.length() ==0){
		return false;
	}
	else{
		this.lastName = lastName;
		return true;
	}
}

bool Student::setAge(int age){
	if (age <=0)
		return false;
	else{
		this.age = age;
		return true;
	}
}
