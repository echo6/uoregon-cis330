#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <string.h>

#define MAX_STRING_LEN 10

int userPickTransfer(char str[]){

	if (strcmp(str, "rock") ==0){
		return 1;
	}
	else if (strcmp(str, "paper") ==0){
		return 2;
	}
	else if (strcmp(str, "scissors") ==0){
		return 3;
	}
	else{
		return -1;
	}
}

int main(){

	int R = 1;
	int P = 2;
	int S = 3;
	int userNum;
	char user[MAX_STRING_LEN];
	srand(time(NULL));

	printf("Enter rock, paper, or scissors: ");
	scanf(" %s", &user);
	userNum = userPickTransfer(user);
	if (userNum == -1){
		printf("Invalid user choice, you must enter rock, paper, or scissors.\n");
		exit(0);
	}
	else{
		printf("You picked %s.\n", user);
	}

	int comp = rand()%3 + 1;
	if (comp == 1){
		printf("Computer picked rock.\n");
	}
	else if (comp ==2){
		printf("Computer picked paper.\n");
	}
	else{
		printf("Computer picked scissors.\n");
	}

	if (userNum == comp){
		printf("This game is a tie.\n");
	}
	else if (userNum ==1){
		if (comp ==2){
			printf("Paper wins.\n");
		}
		else{
			printf("Rock wins.\n");
		}
	}
	else if (userNum == 2){
		if (comp ==3){
			printf("Scissors win.\n");
		}
		else{
			printf("Paper wins.\n");
		}
	}
	else{
		if (comp ==1){
			printf("Rock wins.\n");
		}
		else{
			printf("Scissors win.\n");
		}
	}
	return 0;
}

/*
$ ./a.out
Enter rock, paper, or scissors: rock
You picked rock.
Computer picked paper.
Paper wins.

$ ./a.out
Enter rock, paper, or scissors: scissors
You picked scissors.
Computer picked scissors.
This game is a tie.

$ ./a.out
Enter rock, paper, or scissors: paper
You picked paper.
Computer picked scissors.
Scissors win.

$ ./a.out
Enter rock, paper, or scissors: 2
Invalid user choice, you must enter rock, paper, or scissors.
*/