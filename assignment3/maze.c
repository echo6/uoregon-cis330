#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

enum Direction {RIGHT, UP, LEFT, DOWN};

int isSafe(int size, int row, int col){
    //check position of x and decide whether run or stop
    if (row==0 || col==0){
        return 0;
    }
    else if (row==size-1 || col==size-1){
        return 0;
    }
    else{
        return 1;
    }
}

int findFirstDirection(char **maze, int row, int col){
    //find first direction of x based on initial position
    if (maze[row][col+1] == '.'){
        return RIGHT;
    }
    else if (maze[row-1][col] == '.'){
        return UP;
    } 
    else if (maze[row][col-1] == '.'){
        return LEFT;
    }
    else{
        return DOWN;
    }
}

int findDirection(char **maze, int i, int j, int direction){
    //decide next direction based on position of 'x' and previous direction with order

    //when previous direction was RIGHT
    if (direction==RIGHT){
        if (maze[i][j+1] == '.' && maze[i+1][j] == '@'){
            return RIGHT;
        }
        else if (maze[i+1][j] == '.' && maze[i+1][j-1] == '@'){
            return DOWN;
        }
        else if (maze[i-1][j] == '.' && maze[i][j+1] == '@'){
            return UP;
        }
        else {
            return LEFT;
        }
    }
    //when previous direction was UP
    else if (direction==UP){
        if (maze[i-1][j] == '.' && maze[i][j+1] == '@'){
            return UP;
        }
        else if (maze[i][j+1] == '.' && maze[i+1][j+1] == '@'){
            return RIGHT;
        }
        else if (maze[i][j-1] == '.' && maze[i-1][j] == '@'){
            return LEFT;
        }
        else{
            return DOWN;
        }
    }
    //when previous direction was LEFT
    else if (direction==LEFT){
        if (maze[i][j-1] == '.' && maze[i-1][j] == '@'){
            return LEFT;
        }
        else if (maze[i-1][j] == '.' && maze[i-1][j+1] == '@'){
            return UP;
        }
        else if (maze[i+1][j] == '.' && maze[i][j-1] == '@'){
            return DOWN;
        }
        else{
            return RIGHT;
        }
    }
    //when previous direction was DOWN
    else{
        if (maze[i+1][j] == '.' && maze[i][j-1] == '@'){
            return DOWN;
        }
        else if (maze[i][j-1] == '.' && maze[i-1][j-1] == '@'){
            return LEFT;
        }
        else if (maze[i][j+1] == '.' && maze[i+1][j] == '@'){
            return RIGHT;
        }
        else{
            return UP;
        }
    }
}

void mazeSolve(char **maze, int startRow, int startCol, int size, int direction){
    //current position and previous direction
    int row = startRow;
    int col = startCol;
    int d = direction;

    //print its move and modify its position
    //getting new direction through findDirection() and break if it escapes
    while(true){
        if (d==RIGHT){
            printf("RIGHT\n");
            col++;
        }
        else if (d==UP){
            printf("UP\n");
            row--;
        }
        else if (d==LEFT){
            printf("LEFT\n");
            col--;
        }
        else if (d==DOWN){
            printf("DOWN\n");
            row++;
        }
        if (isSafe(size, row, col)==1){
            d = findDirection(maze, row, col, d);
        }
        else{
            break;
        }
    }
}

int main( int argc, const char* argv[] )
{
    if( argc != 2 ) { 
        //checks for the input file name
        printf( "error; no input file name\n" );
        return 1;
    }
    FILE *filePointer;
    filePointer = fopen( argv[1], "r" );

    //char maze[MAX_MAZE_SIZE][MAX_MAZE_SIZE];
    char str[30];  // 10<= n <= 30 line of txt file
    int numberOfTestCases = 0;
    fscanf( filePointer, "%d\n", &numberOfTestCases );

    for(int testCaseNumber=0; testCaseNumber<numberOfTestCases; testCaseNumber++){
        int mazeSize = 0;
        int row, col, fisrtDirection;
        fscanf(filePointer, "%d\n", &mazeSize);

        //allocate maze
        char **maze = malloc(mazeSize * sizeof(char *));
        for (int i=0; i< mazeSize; i++){
            maze[i] = malloc(mazeSize * sizeof(char));
        }
        //initialize maze array and find index of x
        for (int i=0; i< mazeSize; i++){
            fscanf(filePointer, "%s\n", &str);
            for (int j=0; j<mazeSize;j++){
                if (str[j] == 'x'){
                    row = i;
                    col = j;
                }
                maze[i][j] = str[j];
            }
        }
        printf("ENTER\n");
        
        fisrtDirection = findFirstDirection(maze, row, col);    //set first direction of 'x'
        mazeSolve(maze, row, col, mazeSize, fisrtDirection);    //run maze solve

        //free maze
        if (maze != NULL){
            for (int i=0; i<mazeSize;i++){
                int* currentIntPtr = maze[i];
                free(currentIntPtr);
            }
            free(maze);
        }

        printf("EXIT\n***\n");
    }
    fclose( filePointer );
    return 0;
}
