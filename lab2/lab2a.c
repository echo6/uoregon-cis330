#include <stdio.h>
#include <stdbool.h>

bool arrayEqual(int m, int n, int a[m][n], int b[m][n]){
	int i;
	int j;
	for (i=0; i<m; i++){
		for(j=0; j<n; j++){
			if (a[i][j] != b[i][j]){
				return false;
			}
		}
	}
	return true;
}

int main (){

	bool x;

	int a[3][4] = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}};
	int b[3][4] = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}};
	x = arrayEqual(3, 4, a, b);

	if (x ==1){
		printf("Array a and b are the same\n");
	}
	else{
		printf("Array a and b are not the same\n");
	}

	return 0;
}