#include <stdio.h>
#include <string.h> 
#include <stdlib.h>
#include "square.h"
#include "triangle.h"
#include "diamond.h"

int main(){

	int **mysquare;
	int **mytriangle;
	int **mydiamond;

	int input[32];
	int size;
	int height;

	printf("Problem 1:\n");
	do{
		printf("Please enter the size of the square [2-10]: ");
		scanf(" %s", &input);
		if (atoi(input) == 0){
			size = 0;
		}
		else{
			size = atoi(input);
		}
	}while ((size > 10) || (size < 2));

	allocateNumberSquare(size, &mysquare);
	initializeNumberSquare(size, mysquare);
	printNumberSquare(size, mysquare);
	deallocateNumberSquare(size, &mysquare);

	printf("Problem 2: \n");
	do{
		printf("Please enter the height of the triangle [1-5]: ");
		scanf(" %s", &input);
		if (atoi(input) ==0){
			height = 0;
		}
		else{
			height = atoi(input);
		}
	}while (height>5 | height <1);

	size = height*2 -1;

	allocateNumberTriangle(height, &mytriangle);
	initializeNumberTriangle(height, mytriangle);
	printNumberTriangle(size, mytriangle);
	deallocateNumberTriangle(size, &mytriangle);

	printf("Problem 3: \n");
	do {
		printf("Please enter the size of the diamond [an odd number between 3 and 9 (inclusive)]: ");
		scanf(" %s", &input);
		if (atoi(input) ==0){
			size = 0;
		}
		else{
			size = atoi(input);
		}
	} while ((size < 3) || (size >9) || (size%2==0));

	allocateNumberSquare(size, &mydiamond);
	initializeNumberSquare(size, mydiamond);
	printNumberDiamond(size, mydiamond);
	deallocateNumberSquare(size, &mydiamond);

	return 0;
}

//gcc -std=c11 -g -Wall -o test-square.exe square.c test-square.c
//gcc -std=c11 -g -Wall -o test-all.exe test.c square.c triangle.c diamond.c

/*
create a single program to test all previous problems, and check it for memory leaks using Valgrind.
See the memcheck target in the provided Makefile.

Create a new program (with main) called test.c 
that uses all the square, triangle, and diamond functions above (and includes all header files).

Extend the provided Makefile (and optionally CMakeLists.txt) with a new test-all.exe 
target that combines all the object files in this assignment.

Test for memory leaks with the provided valgrind taget, 
e.g., run "make memcheck" and store the output in a file called memcheck.txt, 
then add, commit, and push that file to your repo 
(in addition to new source files and the modified Makefile).
*/