#include <stdio.h>
#include <string.h> 
#include <stdlib.h>
#include "square.h"
#include "diamond.h"

int main(){

	int **mydiamond;
	char input[32];
	int size;

	printf("Problem 3: \n");
	do {
		printf("Please enter the size of the diamond [an odd number between 3 and 9 (inclusive)]: ");
		scanf(" %s", &input);
		if (atoi(input) ==0){
			size = 0;
		}
		else{
			size = atoi(input);
		}
	} while ((size < 3) || (size >9) || (size%2==0));

	allocateNumberSquare(size, &mydiamond);
	initializeNumberSquare(size, mydiamond);
	printNumberDiamond(size, mydiamond);
	deallocateNumberSquare(size, &mydiamond);
	
	return 0;
}


//gcc -std=c11 -g -Wall -o test-diamond.exe diamond.c test-diamond.c
/*
build on Problem 1 to produce a diamond ASCII shape,
again using a dynamically allocated 2D array. 
reuse the implementation of all but the print function in the square.c 

In a new file diamond.c implement the function from diamond.h.

Create a test program test-diamond.c to allocate, initialize, print the diamond, 
and deallocate the memory used by calling functions in the square.h and diamond.h interfaces.
Format your diamond output as shown below.

Update the Makefile (and optionally the CMakeLists.txt) to compile 
and link the above source files into the test-diamond.exe executable.

Example output
$ ./test-diamond.exe

Problem 3:
Please enter the size of the diamond [an odd number between 3 and 9 (inclusive)]: hmm
Please enter the size of the diamond [an odd number between 3 and 9 (inclusive)]: 4
Please enter the size of the diamond [an odd number between 3 and 9 (inclusive)]: 7
        0 
      0 1 2 
    0 1 2 3 4 
  0 1 2 3 4 5 6 
    0 1 2 3 4 
      0 1 2 
        0 
*/