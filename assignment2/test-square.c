#include <stdio.h>
#include <string.h> 
#include <stdlib.h>
#include "square.h"

int main(){

	int **mysquare;
	char input[32];
	int size;

	printf("Problem 1:\n");
	do{
		printf("Please enter the size of the square [2-10]: ");
		scanf(" %s", &input);
		if (atoi(input) == 0){
			size = 0;
		}
		else{
			size = atoi(input);
		}
	}while ((size > 10) || (size < 2));

	allocateNumberSquare(size, &mysquare);
	initializeNumberSquare(size, mysquare);
	printNumberSquare(size, mysquare);
	deallocateNumberSquare(size, &mysquare);
	
	return 0;
}

//gcc -std=c11 -g -Wall -o test-square.exe square.c test-square.c

/*
rectangular 2-D array of ints (between 0 and 9) 
using dynamic memory allocation and print them in the shape of a square. 

square.c file implements the interface in the square.h (Do not modify the header file in any way)

Create a test program test-square.c, including square.h and a main function
that prompts the user to input the square size, then prints a square of that size.
not crash for invalid input.

The provided Makefile includes a test-square, 
which compiles your sources for this problem into the test-square.exe executable. 
Optionally, you can use cmake, the provided CMakeLists.txt includes a configuration that will build test-square.exe.

Example output:
$ ./test-square.exe

Problem 1:
Please enter the size of the square [2-10]: 11
Please enter the size of the square [2-10]: 10
0 1 2 3 4 5 6 7 8 9 
0 1 2 3 4 5 6 7 8 9 
0 1 2 3 4 5 6 7 8 9 
0 1 2 3 4 5 6 7 8 9 
0 1 2 3 4 5 6 7 8 9 
0 1 2 3 4 5 6 7 8 9 
0 1 2 3 4 5 6 7 8 9 
0 1 2 3 4 5 6 7 8 9 
0 1 2 3 4 5 6 7 8 9 
0 1 2 3 4 5 6 7 8 9 
*/