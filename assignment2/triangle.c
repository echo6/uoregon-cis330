#include <stdio.h>
#include <stdlib.h>
#include "triangle.h"

// Allocate a triangle of height "height" (a 2-D array of int)
void allocateNumberTriangle(const int height, int ***triangle){
	(*triangle) = (int**) malloc(height * sizeof(int *));
	for(int i=0; i<height; i++){
		(*triangle)[i] = (int *) malloc( (2*i+1) * sizeof(int *));
		for (int j=0; j<2*i+1; j++){
			(*triangle)[i][j] = -1;
		}
	}
}

// Initialize the 2-D triangle array
void initializeNumberTriangle(const int height, int **triangle){
	for (int i=0; i<height; i++){
		for(int j=0; j<2*i +1; j++){
			triangle[i][j] = j;
		}
	}
}

// Print a formatted triangle 
void printNumberTriangle(const int size, int **triangle){
	int height = (size+1)/2;
	int k;
	int shift;
	for (int i=0; i< height; i++){
		k = 2*i +1;
		shift = (size-2*i)/2 +1;
		for (int l=0; l<shift;l++){
			printf("  ");
		}
		for (int j=0; j<k; j++){
			printf("%d ", triangle[i][j]);
		}
		for (int m=0; m<shift;m++){
			printf("  ");
		}
		printf("\n");
	}
}

// Free the memory for the 2-D triangle array
void deallocateNumberTriangle(const int size, int ***triangle){
	int height = (size+1)/2;
	if (*triangle != NULL){
		for(int i=0; i<height; i++){
			int* currentIntPtr = (*triangle)[i];
			free(currentIntPtr);
		}
		free(*triangle);
	}
}