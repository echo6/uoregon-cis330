#include <stdio.h>
#include <stdlib.h>
#include "diamond.h"

void printNumberDiamond(const int size, int **square){

	int height = (size+1)/2;
	int k;
	int shift;
	for (int i=0; i < height; i++){
		k = 2*i +1;
		shift = (size-2*i)/2 +1;
		for (int l=0; l<shift; l++){
			printf("  ");
		}
		for (int j=0; j<k; j++){
			printf("%d ", square[i][j]);
		}
		for (int m=0; m<shift; m++){
			printf("  ");
		}
		printf("\n");
	}

	//reverse the triangle and remove the longest row
	for (int i=height-2; i >= 0; i--){
		k = 2*i +1;
		shift = (size-2*i)/2 +1;
		for (int l=0; l<shift; l++){
			printf("  ");
		}
		for (int j=0; j<k; j++){
			printf("%d ", square[i][j]);
		}
		for (int m=0; m<shift; m++){
			printf("  ");
		}
		printf("\n");
	}
}
