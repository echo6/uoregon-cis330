#include <stdio.h>
#include <string.h> 
#include <stdlib.h>
#include "triangle.h"

int main(){

	int **mytriangle;
	char input[32];
	int height;
	int size;

	printf("Problem 2: \n");
	do{
		printf("Please enter the height of the triangle [1-5]: ");
		scanf(" %s", &input);
		if (atoi(input) ==0){
			//not digit
			height = 0;
		}
		else{
			//height = atoi(input);
			height = atoi(input);
		}
	}while (height>5 | height <1);

	size = height*2 -1;
	allocateNumberTriangle(height, &mytriangle);
	initializeNumberTriangle(height, mytriangle);
	printNumberTriangle(size, mytriangle);
	deallocateNumberTriangle(size, &mytriangle);

	return 0;
}

//gcc -std=c11 -g -Wall -o test-triangle.exe triangle.c test-triangle.c

/*
print a (mostly) isosceles ASCII art triangle, 
using single digits between 0 and 9 (inclusive).

In a source file called triangle.c, 
implement the interface in the triangle.h file.

In a separate file called test-triangle.c, 
implement the main function, 
which should include the triangle.h header file 
and call all the functions in triangle.h 
to allocate, initialize, print the triangle, 
and deallocate the memory used. See example output 
for how you should format your triangle when you print it.

Example output
$ ./test-triangle.exe 

Problem 2:
Please enter the height of the triangle [1-5]: 7
Please enter the height of the triangle [1-5]: x
Please enter the height of the triangle [1-5]: 5
          0         
        0 1 2       
      0 1 2 3 4     
    0 1 2 3 4 5 6   
  0 1 2 3 4 5 6 7 8 
*/