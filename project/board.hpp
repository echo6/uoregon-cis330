#ifndef BOARD_H_
#define BOARD_H_

class Board{
public:
	static const int HEIGHT = 600;
	static const int WIDTH = 300;
	static const int ROW = 30;
	static const int COL = 15;
	static const int BLOCK_HEIGHT = HEIGHT / ROW;
	static const int BLOCK_WIDTH = WIDTH / COL;
	//static const int BONUS = 3;
	bool render_score;

	Board();

	void score_increment(int ds){score += ds;}
	int get_score(){return score;}
	void clear_rows();
	bool add_tetromino(Tetromino* t);
	int grid[ROW][COL];

private:
	int score;		//the number of lines cleared
	bool full_row(int row);
	void shift_down(int i);

};

#endif 	//BOARD_H_