#include "game_engine.hpp"
#include "introstate.hpp"
#include "menustate.hpp"
#include "playstate.hpp"

int main(){
	GameEngine gameEngine;
	gameEngine.change_state(Introstate::Instance());
	//gameEngine.change_state(PlayState::Instance());
	gameEngine.execute();

	return 0;
}

//clang++ -framework SDL2 main.cpp game.cpp  well.cpp tetromino.cpp
