#include "background.hpp"

void render_texture(SDL_Texture *texture, SDL_Renderer *render, SDL_Rect dist, SDL_Rect* box){
	SDL_RenderCopy(render, texture, box, &dist);
}

void render_texture(SDL_Texture *texture, SDL_Renderer *render, int x, int y, SDL_Rect* box){
    SDL_Rect dist;
    dist.x = x;
    dist.y = y;
    if (box !=nullptr){
		dist.w = box->w;
		dist.h = box->h;
	} else{
		SDL_QueryTexture(texture, nullptr, nullptr, &dist.w, &dist.h);
	}
	render_texture(texture, render, dist, box);
}

SDL_Texture* renderer_text(const std::string &mes, SDL_Color color,
                           TTF_Font* font, SDL_Renderer *renderer){
	SDL_Surface* surface = TTF_RenderText_Blended(font, mes.c_str(), color);
	SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
	SDL_FreeSurface(surface);
	return texture;
}


SDL_Texture* load_texture(const std::string &input, SDL_Renderer* rend) {
	SDL_Texture* texture = IMG_LoadTexture(rend, input.c_str());
	return texture;
}
