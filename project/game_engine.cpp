#include "game_engine.hpp"
#include "gamestate.hpp"
#include <iostream>

GameEngine::GameEngine(){

	SDL_Init(SDL_INIT_EVERYTHING);
	this->width = 500;
	this->height = 640;

	this->window = SDL_CreateWindow("PP330 Tetris World:", 
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		this->width, this->height, SDL_WINDOW_SHOWN);
	this->renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	this->exitState = false;
}

void GameEngine::execute(){
	while(!this->exitState){
		this->input();
		this->update();
		this->render();
	}
	this->clean_up();
}

void GameEngine::clean_up(){
	//clean up every elements or current element in stack
	while(!this->stack.empty()){
		stack.back()->clean_up(this);
		stack.pop_back();
	}
	SDL_Quit();
}

void GameEngine::change_state(GameState* state){
	//remove current element in stack
	if(!this->stack.empty()){
		stack.back()->clean_up(this);
		stack.pop_back();
	}

	//save the state value to stack
	stack.push_back(state);
	stack.back()->init(this);
}

void GameEngine::push_state(GameState* state){
	//stop the current element running
	if(!stack.empty()){
		stack.back()->pause();
	}
	//push a new state to stack
	stack.push_back(state);
	stack.back()->init(this);
}

void GameEngine::pop_state(){
	if (!stack.empty()){
		stack.back()->clean_up(this);
		stack.pop_back();
	}

	if(!stack.empty()){
		stack.back()->resume();
	}
}

void GameEngine::input(){
	stack.back()->input(this);
}

void GameEngine::update(){
	stack.back()->update(this);
}

void GameEngine::render(){
	stack.back()->render(this);
}

