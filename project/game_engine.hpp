#ifndef GAME_ENGINE_HPP_
#define GAME_ENGINE_HPP_

#include <SDL2/SDL.h>
#include <vector>

class GameState;

class GameEngine{
public:
	GameEngine();

	void clean_up();
	void change_state(GameState* state);
	void push_state(GameState* state);
	void pop_state();

	void execute();
	void input();
	void update();
	void render();

	bool running() {return !exitState;}
	void quit() {exitState = true;}

	SDL_Window* window;
	SDL_Renderer* renderer;
	int width, height;

private:
	std::vector<GameState*> stack;
	bool exitState;
};

#endif //GAME_ENGINE_HPP_