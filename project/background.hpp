#ifndef BACKGROUND_HPP_
#define BACKGROUND_HPP_


#include <SDL2/SDL.h>
#include <SDL2_ttf/SDL_ttf.h>
#include <SDL2_image/SDL_image.h>
#include <string>



void render_texture(SDL_Texture *texture, SDL_Renderer *render,
                    SDL_Rect dist, SDL_Rect *box = nullptr);
void render_texture(SDL_Texture *texture, SDL_Renderer *render,
                    int x, int y, SDL_Rect *box = nullptr);

SDL_Texture* renderer_text(const std::string &mes, SDL_Color color,
                           TTF_Font* font, SDL_Renderer *renderer);
SDL_Texture* load_texture(const std::string &input, SDL_Renderer *rend);
#endif //BACKGROUND_HPP_

