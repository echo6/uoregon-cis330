#include "menustate.hpp"
#include "playstate.hpp"
#include "background.hpp"

Menustate Menustate::_menu;

void Menustate::init(GameEngine* game){
	play = false;
	exit =false;

	TTF_Init();// initialize font with white color
	white = {255, 255, 255};

	//textures
	font = TTF_OpenFont("resource/font/bitwise.ttf", 16);
	font_for_title = TTF_OpenFont("resource/font/bitwise.ttf", 32);

	title_font = renderer_text("Tetris", white, font_for_title, game->renderer);
	play_font = renderer_text("Play", white, font, game->renderer);
	quit_font = renderer_text("Quit", white, font, game->renderer);
	copy_font = renderer_text("Created by Edward Cho and Jenney Lee", white, font, game->renderer);

	//text position.
	SDL_QueryTexture(title_font, nullptr, nullptr, &title_width, &title_height);
	SDL_QueryTexture(play_font, nullptr, nullptr, &play_width, &play_height);
	SDL_QueryTexture(quit_font, nullptr, nullptr, &quit_width, &quit_height);

	current_selected = 0;
	items =2;

}

void Menustate::clean_up(GameEngine* game){
	TTF_CloseFont(font);
	SDL_DestroyTexture(title_font);
	SDL_DestroyTexture(play_font);
	SDL_DestroyTexture(quit_font);

	IMG_Quit();
}
/*
void Menustate::pause(){}
void Menustate::resume(){}
void Menustate::reset(){}
*/

//event series 
void Menustate::input(GameEngine* game){
	SDL_Event event;
	while(SDL_PollEvent(&event)){
		//SDL_QUIT makes you to exit by pressing X button or F4
		if (event.type == SDL_QUIT){
			exit = true;
		}
		// button pressed
		if (event.type ==SDL_KEYDOWN){
			switch (event.key.keysym.sym){
				case SDLK_ESCAPE:
					exit = true;
					break;
				case SDLK_UP:
					select_up();
					break;
				case SDLK_DOWN:
					select_down();
					break;
				case SDLK_RETURN:
					if (current_selected == 0){
						play = true;
					} else if (current_selected == 1){
						exit = true;
					} break;
				default:
					break;
			}
		}
	}
}

void Menustate::update(GameEngine* game){
	if (this->play){
		game->push_state(PlayState::Instance());
	} else if (this->exit) {
		game->quit();
	}
}

void Menustate::render(GameEngine* game){
	//clear screen 
	SDL_SetRenderDrawColor(game->renderer, 0,0,0,1);
	SDL_RenderClear(game->renderer);
	int space = 30;
	int width, height;

	//Tetris Title
	SDL_QueryTexture(title_font, nullptr, nullptr, &width, &height);
	render_texture(title_font, game->renderer,(game->width - this->title_width)/2, 
		(game->height - this->title_height)/2 - space*2);

	//emphasize the selected item. -> 전에는 언더라인, 다른 방법으로 ㄲ
	if (current_selected == 0 ){
		TTF_SetFontStyle(font, TTF_STYLE_BOLD);
		//TTF_SetFontStyle(font, TTF_STYLE_UNDERLINE);
		play_font = renderer_text("Play", white, font, game->renderer);
	}
	else if (current_selected == 1){
		TTF_SetFontStyle(font, TTF_STYLE_BOLD);
		//TTF_SetFontStyle(font, TTF_STYLE_UNDERLINE);
		quit_font = renderer_text("Quit", white, font, game->renderer);
	}

	//menu items (centered)
	render_texture(play_font, game->renderer, (game->width - play_width)/2,
	 (game->height - play_height)/2);
	render_texture(quit_font, game->renderer, (game->width - quit_width)/2,
	 (game->height - quit_height)/2 + space);

	//remove emphasize after nonselection
	if (current_selected == 0){
		TTF_SetFontStyle(font, TTF_STYLE_NORMAL);
		play_font = renderer_text("Play", white, font, game->renderer);
	} else if (current_selected == 1){
		TTF_SetFontStyle(font, TTF_STYLE_NORMAL);
		quit_font = renderer_text("Quit", white, font, game->renderer);
	}

	SDL_QueryTexture(copy_font, nullptr, nullptr, &width, &height);
	render_texture(copy_font, game->renderer,(game->width - this->title_width)/3 - space, 
		(game->height - this->title_height)/2 + space*5);

	//swap buffers -> 뭘 하는지 잘 모르겠음.
	SDL_RenderPresent(game->renderer);
}

void Menustate::select_up(){
	if(current_selected > 0){
		current_selected = (current_selected-1)%items;
	}
}
void Menustate::select_down(){
	if (current_selected < items -1){
		current_selected = (current_selected+1)%items;
	}	
}

