#include "tetromino.hpp"
#include "board.hpp"

const int Tetromino::coords_shape[8][4][2] = {
	//coordinates of each blocks based on (0,0) as central block
	//first elements of each block info are the very low and left
	{{0,-1}, {0,0}, {0,1}, {0,2}},	//blcok I
	{{0,0}, {1,0}, {0,1}, {1,1}},	//blcok O
	{{-1,0}, {0,0}, {1,0}, {0,1}},	//blcok T
	{{1, -1}, {0,-1}, {0,0}, {0,1}},//block J
	{{-1,-1}, {0,-1}, {0,0}, {0,1}},//block L
	{{0,-1}, {0,0}, {-1,0}, {-1,1}},//block Z
	{{0,-1}, {0,0}, {1,0}, {1,1}},	//block S
	{{0,0}, {0,0}, {0,0}, {0,0}}	//block B, Bonus block, item block
};

Tetromino::Tetromino(int new_shape){
	this->shape = new_shape;
	this->free_fall = false;
	this->speed_up = false;
	this->status = INACTIVE;
	this->direction = NONE;

	//if shape is decided, save a single shape's info to coords
	this->coords = new int[4][2];
	for (int i=0; i <4; i++){
		coords[i][0] = coords_shape[this->shape][i][0];
		coords[i][1] = coords_shape[this->shape][i][1];
	}
}

void Tetromino::rotate_right(){
	for (int i=0; i <this->SIZE; i++){
		int tmp = this->coords[i][0];
		this->coords[i][0] = coords[i][1];
		this->coords[i][1] = -tmp;
	}
}

void Tetromino::rotate_left(){
	for (int i=0; i<this->SIZE; i++){
		int tmp = this->coords[i][0];
		this->coords[i][0] = -coords[i][1];
		this->coords[i][1] = tmp;
	}
}

void Tetromino::shadow(Board *board, int shadow_y[]){
	int copy_y = y;				//save the original y for shadow simulation
	Status copy_status = status;	//save the original status for shadow simulation
	//from block's point, keep going down(decrease y) while block hit the land
	while(!this->has_landed()){
		for (int i=0; i<this->SIZE; i++){
			//when it hits the ceiling or the bottom
			if(this->get_block_y(i) == board->ROW 
				|| board->grid[this->get_block_y(i)][this->get_block_x(i)]!= -1){
				this->status_land();	//change status to landed
				this->y--;
				break;
			}
		}
		if(!this->has_landed()){
			this->y++;
		}
	}

	for (int i=0; i<this->SIZE; i++){
		shadow_y[i] = this->get_block_y(i);
	}
	y=copy_y;				//back to the original value
	status = copy_status;	//back to the original value
}
