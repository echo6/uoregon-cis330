#ifndef TETROMINO_HPP_
#define TETROMINO_HPP_

class Board;

class Tetromino{
public:
	enum Shapes{I, O, T, J, L, Z, S, B};
	enum Status{INACTIVE, WAITING, FALLING, LANDED};
	enum Direction{NONE = 0, LEFT = -1, RIGHT = 1};
	static const int coords_shape[8][4][2];
	static const int SIZE = 4;

	Tetromino(int type);	//constructor
	int get_shape(){return shape;}

	void set_position(int new_x, int new_y){x = new_x; y = new_y;}
	void set_block_x(int shape, int new_x){x = new_x - coords[shape][0];}
	void set_block_y(int shape, int new_y){y = new_y - coords[shape][1];}

	//upper left position of x
	int get_block_x(int i){return x + coords[i][0];}
	//upper left position of y
	int get_block_y(int i){return y + coords[i][1];}
	void add_to_x(int offset){x +=offset;}

	bool has_landed(){return status == LANDED;}
	void status_land(){status = LANDED;}
	void status_drop(){status = FALLING;}

	void rotate_right();
	void rotate_left();
	void shadow(Board* board, int shadow_y[]);

	Direction direction;
	bool free_fall;
	int x, y;		//position of (0,0)
	bool speed_up;
	bool shift;
	bool rotate;

private:
	Status status;
	int shape;	//shape of tetromino block
	int (*coords)[2];	//each block's coordinats info
};

#endif // TETROMINO_H_
