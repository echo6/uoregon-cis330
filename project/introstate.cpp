#include "introstate.hpp"
#include "menustate.hpp"
#include "background.hpp"
#include <iostream>

Introstate Introstate::_intro;

void Introstate::init(GameEngine* game){
	texture = load_texture("resource/images/intro.png", game->renderer);
	exit = false;
	a = 1;
	ani = FADE_IN;
}

void Introstate::clean_up(GameEngine* game){
	IMG_Quit();
}

void Introstate::pause(){}
void Introstate::resume(){}
void Introstate::reset(){}

void Introstate::input(GameEngine* game){
	SDL_Event e;
	while (SDL_PollEvent(&e))
	{
		if(e.type ==SDL_QUIT){ // x or F4
			exit = true;
		}

		if (e.type == SDL_KEYDOWN)	// key up down
		{ 
			switch (e.key.keysym.sym)
			{
				case SDLK_ESCAPE:
					exit = true;
					break;
				default:
					break;
			}
		}
	}
}

void Introstate::update(GameEngine* game){
	if (exit){
		game->quit();
	}
	if (a == 0){
		game->push_state(Menustate::Instance());
	}
}

void Introstate::render(GameEngine* game){
	//clear 
	SDL_SetRenderDrawColor(game->renderer, 0,0,0,1);
	SDL_RenderClear(game->renderer);

	render_logo(game);

	//swap uffers.
	SDL_RenderPresent(game->renderer);
}

void Introstate::render_logo(GameEngine* game){
	if(ani == FADE_IN){
		a+=3;
		if (a >=255) {
			a =255;
			ani = STILL;
		} 
	}
	else if (ani == STILL) {
		SDL_Delay(1000);
		ani = FADE_OUT;
	} 
	else if (ani == FADE_OUT){
		a-=3;
		if(a<=0){
			a = 0;
			ani = FADE_IN;
		}
	}

	SDL_SetTextureAlphaMod(texture, a);

	int logo_width, logo_height;
	SDL_QueryTexture(texture, nullptr, nullptr, &logo_width, &logo_height);
	int x = game->width /2 - logo_width /2;
	int y = game->height /2 - logo_height /2;

	render_texture(texture, game->renderer, x,y);

}
