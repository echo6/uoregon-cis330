#ifndef MENUSTATE_HPP_
#define MENUSTATE_HPP_

#include <SDL2/SDL.h>
#include <SDL2_ttf/SDL_ttf.h>

#include "gamestate.hpp" //gamestate

class Menustate: public GameState {
public:
	void init(GameEngine* game);
	void clean_up(GameEngine* game);

	void pause() {}
	void resume(){}
	void reset() {}

	void input(GameEngine* game);
	void update(GameEngine* game);
	void render(GameEngine* game);

	//navigation in menu items by key board.
    
	void select_up();
	void select_down();

	static Menustate* Instance() {return &_menu;}

protected:
	Menustate(){}

private:
	static Menustate _menu;

	bool play;
	bool exit;

	//need to implement font texture
	SDL_Color       white;
	TTF_Font*       font;
	TTF_Font* 		font_for_title;
	SDL_Texture* 	copy_font;
	SDL_Texture*    title_font;
    SDL_Texture*    play_font;
    SDL_Texture*    quit_font;

    int title_width, title_height;
    int play_width, play_height;
    int quit_width, quit_height;

	//selected menu item.
	int current_selected;

	//items
	int items;

};

#endif //MENUSTATE_HPP_
