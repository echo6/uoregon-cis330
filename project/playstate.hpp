#ifndef PLAYSTATE_HPP_
#define PLAYSTATE_HPP_

#include <SDL2/SDL.h>
#include <SDL2_ttf/SDL_ttf.h>
#include <SDL2_image/SDL_image.h>

#include "gamestate.hpp"

class Board;
class Tetromino;

class PlayState: public GameState{
public:

	//static const int NGRID = 7;
	static const int OFFSET = 20;

	void init(GameEngine* game);
	void clean_up(GameEngine* game);

	void pause() {pauseState = true;}
	void resume() {pauseState = false;}
	void reset();
	void input(GameEngine* game);
	void update(GameEngine* game);
	void render(GameEngine* game);

	static PlayState* Instance() {return &myPlayState;}

protected:
	PlayState() {}
private:
	static PlayState myPlayState;

	//Game objects
	Board* myboard;
	Tetromino* currTetro;
	Tetromino* nextTetro;
	Tetromino* bonusTetro;

	void release_tetromino();
	void release_bonus();
	void draw_tetromino(GameEngine* game, int x, int y, int k, SDL_Rect clips[]);
	float frame_rate(GameEngine* game, int* last_time, int* this_time);

	//Frame rate
	float accerlation;
	float time_till_drop;
	float time_counter;
	int this_time;
	int last_time;

	//button coordinates
	int newgamex1;
	int newgamey1;
	int newgamey2;

	bool pauseState;
	bool exit;
	bool quit_game_up;
	bool quit_game_down;
	bool new_game_up;
	bool new_game_down;
	bool game_over;

	//Texture
	SDL_Texture* block_texture;

	//Font
	SDL_Color white;
	TTF_Font* font;

	//Texture
	SDL_Texture* image_pause;
	SDL_Texture* image_pause_info;
	SDL_Texture* image_tetris;
	SDL_Texture* image_score_text;
	SDL_Texture* image_score;
	SDL_Texture* image_newgame;
	SDL_Texture* image_quit;
	SDL_Texture* image_gameover;
	SDL_Texture* image_copyright;

};

#endif  //PLAYSTATE_H_
