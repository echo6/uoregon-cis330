#include "tetromino.hpp"
#include "board.hpp"

Board::Board(){
	this->score = 0;
	this->render_score = true;
	for (int i=0; i<ROW;i++){
		for (int j=0; j<COL; j++){
			this->grid[i][j] = -1;		//empty =>-1
		}
	}
}

bool Board::full_row(int row){
	//return true if the whole row is occupied
	for (int i=0; i<COL; i++)
		if (this->grid[row][i] == -1)
			return false;
	return true;
}

void Board::shift_down(int i){
	//when rows are cleared and there's empty space, shift the left thing above down
	for (int r=i; r>0; r--)
		for (int c=0; c<COL;c++)
			this->grid[r][c] = this->grid[r-1][c];
}

void Board::clear_rows(){
	for (int r=ROW-1; r>=0; r--){
		if(!this->full_row(r))
			continue;
		this->shift_down(r);
		r++;
		this->score_increment(1);
		this->render_score = true;
	}
}

bool Board::add_tetromino(Tetromino* t){
	for (int i=0; i<t->SIZE; i++){
		int x = t->get_block_x(i);
		int y = t->get_block_y(i);
		if (y<=0)
			return false;
		else
			this->grid[y][x] = t->get_shape();
	}
	return true;
}
