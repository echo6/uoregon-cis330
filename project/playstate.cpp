#include "game_engine.hpp"
#include "background.hpp"
#include "playstate.hpp"
#include "tetromino.hpp"
#include "board.hpp"

#include <random>

namespace {
	std::random_device rdm;
	//sstd::mt19937 gen(rdm());
}

PlayState PlayState::myPlayState;

void PlayState::init(GameEngine* game){
	//game objects.
	myboard = new Board();
	currTetro = new Tetromino(rdm()%7);
	nextTetro = new Tetromino(rdm()%7);
	bonusTetro = new Tetromino(7);

	this->accerlation = 0.005f;
	this->time_till_drop = 0.3f;
	this->time_counter = 0.0f;
	this->this_time = 0;
	this->last_time = 0;

	this->newgamex1 = OFFSET + myboard->WIDTH + myboard->BLOCK_WIDTH;
	this->newgamey1 = myboard->HEIGHT-4*myboard->BLOCK_HEIGHT;
	this->newgamey2 = myboard->HEIGHT - 6*myboard->BLOCK_HEIGHT;
	
	this->pauseState = false;
	this->exit = false;
	this->quit_game_up = false;
	this->quit_game_down = false;
	this->new_game_up = false;
	this->new_game_down = false;
	this->game_over = false;

	block_texture = load_texture("resource/sprites/block.bmp", game->renderer);

	white = {255, 255, 255};
	font = TTF_OpenFont("resource/font/bitwise.ttf", 20);

	image_pause = renderer_text("Pause", white, this->font, game->renderer);
	image_pause_info = renderer_text("Pause: p", white, this->font, game->renderer);
	image_tetris = renderer_text("Tetris", white, this->font, game->renderer);
	image_score_text = renderer_text("Score: ", white, this->font, game->renderer);
	image_score = renderer_text(std::to_string(myboard->get_score()), white, this->font, game->renderer);
	image_newgame = renderer_text("New game: n", white, this->font, game->renderer);
	image_quit = renderer_text("Quit: q", white, this->font, game->renderer);
	image_gameover = renderer_text("Game Over!", white, this->font, game->renderer);
	image_copyright = renderer_text("Copyright. 2018. Edward Cho, Jenney Lee\n All Rights Reserved", white, this->font, game->renderer);

	currTetro->set_position(static_cast<int>(myboard->COL/2), 0);
	nextTetro->set_position(myboard->COL + 5, static_cast<int>(0.3 * myboard->ROW));
}

void PlayState::clean_up(GameEngine* game){

	TTF_CloseFont(this->font);
	SDL_DestroyTexture(image_pause);
	SDL_DestroyTexture(image_tetris);
	SDL_DestroyTexture(image_score_text);
	SDL_DestroyTexture(image_score_text);
	SDL_DestroyTexture(image_newgame);
	SDL_DestroyTexture(image_quit);
	SDL_DestroyTexture(image_gameover);

	IMG_Quit();
	SDL_DestroyRenderer(game->renderer);
	SDL_DestroyWindow(game->window);
	SDL_Quit();
}

void PlayState::reset(){
	//make board empty
	for (int i=0; i<this->myboard->ROW; i++)
		for(int j=0; j<this->myboard->COL;j++)
			myboard->grid[i][j] = -1;

	delete myboard;
	delete currTetro;
	delete nextTetro;

	myboard = new Board();
	currTetro = new Tetromino(rdm()%7);
	nextTetro = new Tetromino(rdm()%7);
	currTetro->set_position(static_cast<int>(myboard->COL/2), 0);
	nextTetro->set_position(myboard->COL+5, static_cast<int>(0.3 * myboard->ROW));

	this->new_game_down = false;
	this->new_game_up = false;
	this->game_over = false;
	this->pauseState = false;
}

void PlayState::input(GameEngine* game){
	SDL_Event e;
	while(SDL_PollEvent(&e)){
		//'x' is clicked to quit the game
		if(e.type==SDL_QUIT || e.type==SDLK_q){
			this->exit = true;
		}

		//key is pressed
		if(e.type==SDL_KEYDOWN){
			//'p' s pressed
			if(e.key.keysym.sym == SDLK_p){
				if (this->pauseState){
					resume();
				}
				else{
					pause();
				}
			}
			if (!this->pauseState && !this->currTetro->free_fall){
				switch (e.key.keysym.sym){
					case SDLK_ESCAPE:
						this->exit = true;
						break;
					case SDLK_LEFT:
						currTetro->direction = currTetro->LEFT;
						currTetro->shift = true;
						break;
					case SDLK_RIGHT:
						currTetro->direction = currTetro->RIGHT;
						currTetro->shift = true;
						break;
					case SDLK_UP:
						if (currTetro->get_shape() !=1 && currTetro->get_shape() !=7)
							currTetro->rotate = true;
						break;
					case SDLK_DOWN:
						currTetro->speed_up = true;
						break;
					case SDLK_SPACE:
						currTetro->free_fall = true;
						break;
					
					case SDLK_q:		//quit Game
						this->quit_game_up = true;
						this->quit_game_down = true;
						break;
					case SDLK_n:
						this->new_game_up = true;		//new game
						this->new_game_down = true;
						break;
					default:
						break;
				}
			}
		}

		//Key is released.
		if(e.type ==SDL_KEYUP){
			switch(e.key.keysym.sym){
				case SDLK_DOWN:
					currTetro->speed_up = false;
					break;
				default:
					break;
			}
		}

		//Mouse cursor is not shown on the window
		if(e.type ==SDL_MOUSEMOTION){
			// do not show cursor inside the window
			SDL_ShowCursor(0);
		}
	}
}

void PlayState::release_tetromino() {
	Tetromino* new_tetro = new Tetromino(rdm()%7);
	new_tetro->set_position(this->nextTetro->x, this->nextTetro->y);

	delete currTetro;	//delete current tetro who will be landed
	currTetro = nextTetro;
	currTetro->set_position(myboard->COL/2, 0);
	nextTetro = new_tetro;
	currTetro->status_drop();
}

void PlayState::update(GameEngine* game){
	//key 'n' clicked and start new game
	if(this->new_game_up && this->new_game_down){
		reset();
	}

	//quit or exit game
	if((this->quit_game_up &&this->quit_game_down)|| this->exit){
		game->quit();
	}

	//Pause the game
	if (this->game_over || this->pauseState){
		return;
	}

	//Tetro has landed.
	if(this->currTetro->has_landed()){
		currTetro->free_fall = false;
		//check whether adding tetro finishing the game.
		if (!myboard->add_tetromino(this->currTetro)){
			this->game_over = true;
			return;	//quit function
		}
		release_tetromino();	//drop the next tetro
	}
	else if (this->currTetro->free_fall){
		this->currTetro->y++;	//MAX speed
	}
	//Rotate and move check whether it is possible to rotate and move
	else{
		//Rotatation checking
		if(this->currTetro->rotate)
			currTetro->rotate_left();
		this->currTetro->add_to_x(this->currTetro->direction);		//update position afeter rotation

		if(currTetro->speed_up){
			this->time_till_drop = 0.02f;
		}
		else{
			this->time_till_drop = 0.3f - myboard->get_score() * this->accerlation;
		}

		time_counter += this->frame_rate(game, &last_time, &this_time);

		if (time_counter >= this->time_till_drop){
			this->currTetro->y++;
			this->time_counter = 0.0f;
		}
	}
	//Collision detecting. possible position or not? if not, cancel move
	for (int i=0; i<currTetro->SIZE; i++){
		int x = this->currTetro->get_block_x(i);
		int y = this->currTetro->get_block_y(i);

		//when tetromino out of board in horizontal way
		if (x <0 || x >= myboard->COL){
			//caused by rotate problem
			if (this->currTetro->rotate)
				this->currTetro->rotate_right();

			//caused by moving
			if(this->currTetro->shift)
				this->currTetro->x -= this->currTetro->direction;
			break;
		}
		//when tetromino out of board in vertical way: touching ground
		else if (y >=myboard->ROW){
			this->currTetro->status_land();
			this->currTetro->set_block_y(i, myboard->ROW -1);
		}
		else if (y >= 0){
			if (myboard->grid[y][x] != -1){
				if (this->currTetro->shift || this->currTetro->rotate){
					if(this->currTetro->shift){
						this->currTetro->x -= this->currTetro->direction;
					}
					if (this->currTetro->rotate){
						this->currTetro->rotate_right();
					}
					break;
				}
				else{
					this->currTetro->y--;
					this->currTetro->status_land();
				}
			}
		}
	}
	myboard->clear_rows();
	this->currTetro->rotate = false;
	this->currTetro->shift = false;
	this->currTetro->direction =this->currTetro->NONE;		//NONE status
}

void PlayState::render(GameEngine* game){
	//Clean Window
	SDL_SetRenderDrawColor(game->renderer, 0, 0, 0, 1);
	SDL_RenderClear(game->renderer);

	int x = (this->nextTetro->x -3)* myboard->BLOCK_WIDTH;
	int y = this->OFFSET;

	render_texture(image_tetris, game->renderer, x, y);

	//Pause text
	if (this->pauseState)
		render_texture(this->image_pause,game->renderer, x, y+50);

	render_texture(image_score_text, game->renderer, x, y+myboard->BLOCK_WIDTH);
	//render score
	if(myboard->render_score){
		this->image_score = renderer_text(std::to_string(myboard->get_score()), this->white, this->font, game->renderer);
		myboard->render_score = false;
	}
	render_texture(this->image_score, game->renderer, x+60, y + myboard->BLOCK_WIDTH);

	int _x, _y;
	int wi, he;
	SDL_QueryTexture(block_texture, nullptr, nullptr, &wi, &he);
	SDL_Rect clips[7];

	for (int i=0; i<7; i++){
		clips[i].x = 0;
		clips[i].y = i*24;
		clips[i].w = 20;
		clips[i].h = 20;
	}

	//Draw tetromino block
	for (int i=0; i<this->currTetro->SIZE; i++){
		_x = this->currTetro->get_block_x(i) * myboard->BLOCK_WIDTH + this->OFFSET;
		_y = this->currTetro->get_block_y(i) * myboard->BLOCK_HEIGHT + this->OFFSET;
		draw_tetromino(game, _x, _y, this->currTetro->get_shape(), clips);
	}

	//Draw Shadow of tetromino
	int shadow_y[4];
	this->currTetro->shadow(myboard, shadow_y);
	for (int i=0; i<this->currTetro->SIZE; i++){
		if (shadow_y[i] < 0)
			break;
		int x = this->currTetro->get_block_x(i) * myboard->BLOCK_WIDTH + this->OFFSET;
		int y = shadow_y[i] * myboard->BLOCK_HEIGHT + this->OFFSET;

		//Draw block
		SDL_SetRenderDrawColor(game->renderer, 180, 180, 180, 255);
		SDL_Rect shadow_block = {x, y, myboard->BLOCK_WIDTH, myboard->BLOCK_HEIGHT};
		SDL_RenderFillRect(game->renderer, &shadow_block);
	}

	if (!this->game_over){
		//Draw next tetromino
		for (int i=0; i < this->nextTetro->SIZE; i++){
			_x = this->nextTetro->get_block_x(i) * myboard->BLOCK_WIDTH;
			_y = this->nextTetro->get_block_y(i) * myboard->BLOCK_HEIGHT;
			draw_tetromino(game, _x, _y, this->nextTetro->get_shape(), clips);
		}
	}

	//virtual boards simulation
	for (int i=0; i<myboard->ROW; i++){
		for (int j=0; j<myboard->COL; j++){
			if (myboard->grid[i][j] != -1){
				_x = j* myboard->BLOCK_WIDTH + this->OFFSET;
				_y = i* myboard->BLOCK_HEIGHT + this->OFFSET;
				draw_tetromino(game, _x, _y, myboard->grid[i][j], clips);
			}
		}
	}

	SDL_SetRenderDrawColor(game->renderer, 180, 180, 180, 255);

	//Left border
	SDL_RenderDrawLine(game->renderer, this->OFFSET, this->OFFSET, 
		this->OFFSET, this->OFFSET+myboard->HEIGHT);

	//Right border
	SDL_RenderDrawLine(game->renderer, this->OFFSET+myboard->WIDTH,
	 this->OFFSET, this->OFFSET+myboard->WIDTH, this->OFFSET+myboard->HEIGHT);

	//upper border
	SDL_RenderDrawLine(game->renderer, this->OFFSET,
	 this->OFFSET, this->OFFSET+myboard->WIDTH, this->OFFSET);

	//bottom border
	SDL_RenderDrawLine(game->renderer, this->OFFSET,
	 this->OFFSET+myboard->HEIGHT, this->OFFSET+myboard->WIDTH, this->OFFSET+myboard->HEIGHT);

	if (this->game_over){
		render_texture(this->image_gameover, game->renderer, this->newgamex1, 
			game->height-this->newgamey1 + 4*myboard->BLOCK_WIDTH);
	}

	render_texture(this->image_pause_info, game->renderer, this->newgamex1+10, 
		this->newgamey2 - 4 * this->myboard->BLOCK_HEIGHT+10);
	render_texture(this->image_newgame, game->renderer, this->newgamex1+10, this->newgamey2+10);
	render_texture(this->image_quit, game->renderer, this->newgamex1+10, 
		this->newgamey2 + 4 * this->myboard->BLOCK_HEIGHT + 10);

	//Swap buffers
	SDL_RenderPresent(game->renderer);
}

void PlayState::release_bonus(){
	Tetromino* bonus = new Tetromino(7);
	Tetromino* new_tetro = new Tetromino(rdm()%7);
	new_tetro->set_position(this->nextTetro->x, this->nextTetro->y);

	delete currTetro;
	currTetro = bonus;
	currTetro->set_position(myboard->COL/2, 0);
	currTetro->status_drop();
}

void PlayState::draw_tetromino(GameEngine* game,
        int x, int y, int k, SDL_Rect clips[]){
	render_texture(block_texture, game->renderer, x, y, &clips[k]);
}

float PlayState::frame_rate(GameEngine* game, int* last_time, int* this_time){
	//time since SDL_Init() from previous 
	*last_time = *this_time;

	//time since SDL_Init()
	*this_time = SDL_GetTicks();

	return ((*this_time - *last_time) / 1000.0f);		//transit milliseconds to seconds
}

