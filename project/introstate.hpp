#ifndef INTROSTATE_HPP_
#define INTROSTATE_HPP_
#include <SDL2/SDL.h>
#include <SDL2_image/SDL_image.h>

#include "gamestate.hpp"

class Introstate : public GameState {
public:
	void init(GameEngine* game);
	void clean_up(GameEngine* game);

	void pause();
	void resume();
	void reset();

	void input(GameEngine* game);
	void update(GameEngine* game);
	void render(GameEngine* game);

	static Introstate* Instance() {return &_intro; }
	void render_logo(GameEngine* game);

protected:
	Introstate() { }

private:
	static Introstate _intro;
	bool exit;

	SDL_Texture* texture;
	int a;
	enum Animation {FADE_IN, STILL, FADE_OUT};
	Animation ani;


};

#endif //INTROSTATE_HPP_
