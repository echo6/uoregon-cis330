 /*
 * test-game.cpp
 *
 * Created on: Feb 14, 2014
 *      Author: Cho
 */

#include <iostream>
#include <stdlib.h>
using namespace std;
#include "game.hpp"

int main(){

	int row;
	int col;
	int numberofstep;

	cout<<"Please enter the size of the grid (int int): ";
	cin>>row>>col;
	cout<<"Please enter the number of steps (int): ";
	cin >>numberofstep;

	Game mygame(row, col, numberofstep);
	mygame.gameRunning();

	return 0;
}
