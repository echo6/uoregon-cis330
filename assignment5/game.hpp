/*
 * game.hpp
 *
 *  Created on: Feb 14, 2014
 *      Author: Cho
 */
#ifndef GAME_HPP_
#define GAME_HPP_

#include <string>
using namespace std;


//grid and overal game operator
class Game{

public:
	Game(int initRow, int initCol, int initNumberOfStep); //random init with empty, sheep, wolf, farmer.
	~Game();

	void gameRunning();			//Run step() as much as numberOfStep

protected:
	int row;
	int col;
	int numberOfStep;
	std::string gridData;
	std::string moveChecker;	//to prohibit farmer's move in single step

private:
	void step();	//Perform a transition according to the rules above for each time step
	void printGrid();	//print grid

	int fixedRow(int i);			//fix row value if it is over range
	int fixedCol(int j);			//fix col value if it is over range
	void removeCell(int i, int j);	//make grid[i][j] empty
	char getChar(int i, int j);		//get char of grid[i][j]
	void setChar(int i, int j, char name);	//set grid[i][j] as name
	void initializeMoveChecker();	//initialize moveChec
	int numberOfSpecies(int i, int j, char name);	//count the number of name arround grid[i][j]
	
	void overpopulation(int i, int j, char name);
	void predation(int i, int j);
	void killedByFarmer(int i, int j);
	void starvation(int i, int j);
	void farmerMove(int i, int j);
	void reproduction(int i, int j);

};

#endif /* GAME_HPP */
