/*
 * game.cpp
 *
 * Created on: Feb 14, 2014
 *      Author: Cho
 */
#include "game.hpp"
#include <string>
#include <stdlib.h>
#include <time.h>
#include <iostream>
using namespace std;

Game::Game(int initRow, int initCol, int initNumberOfStep){
	//set szie of grid and number of step
	this->row = initRow;
	this->col = initCol;
	this->numberOfStep = initNumberOfStep;
	int decision = 0;	//for random number set up
	srand (time(NULL));
	for (int i=0; i<this->row; i++){
		for (int j=0; j<this->col; j++){
			this->moveChecker += '.';	//set movechecker
			decision = rand()%11;	//get random number
			if (decision==10){
				this->gridData += 'F';	//allocate farmer
			}
			else if (decision%2 ==0){
				this->gridData += 'S';		//allocate sheep
			}
			else if (decision ==1){
				this->gridData += 'W';		//allocate wolf
			}
			else{
				this->gridData += '.';	//allocate '.' empty cell
			}
		}
	}
	this->printGrid();
}

Game::~Game(){//destructor
	this->row=0;
	this->col=0;
	this->numberOfStep=0;
	this->gridData.clear();	//delete string
}

void Game::gameRunning(){
	//Run step() as much as numberOfStep
	for (int i=0; i<this->numberOfStep;i++){
		cout<<"Step "<<i+1<<":"<<endl;
		this->step();
		this->printGrid();
	}
}

void Game::step(){
	//single step of game
	for (int i=0; i < this->row; i++){
		for (int j=0; j < this->col; j++){
			//when grid[i][j] is Sheep
			if (this->getChar(i,j) == 'S'){
				this->overpopulation(i, j, 'S');
				this->predation(i, j);
			}
			//when grid[i][j] is Wolf
			else if(this->getChar(i,j) == 'W'){
				this->overpopulation(i, j, 'W');
				this->killedByFarmer(i, j);
				this->starvation(i, j);
			}
			//when grid[i][j] is Farmer
			else if(this->getChar(i,j) =='F'){
				this->farmerMove(i, j);
			}
			//when grid[i][j] is empty
			else if(this->getChar(i,j) =='.'){
				this->reproduction(i, j);
			}
		}
	}
	this->initializeMoveChecker();
}

void Game::printGrid(){
	//print grid
	for (int i=0;i<this->row; i++){
		for (int j=0; j<this->col; j++){
			cout<<this->getChar(i,j);
		}
		cout<<endl;
	}
	cout<<endl;
}

int Game::fixedRow(int i){
	//if i is out of range, fix it
	if (i <0){
		return this->row -1;
	}
	if (i >= this->row){
		return 0;
	}
	return i;
}

int Game::fixedCol(int j){
	//if j is out of range, fix it
	if (j <0){
		return this->col-1;
	}
	if (j >=this->col){
		return 0;
	}
	return j;
}

void Game::removeCell(int i, int j){
	//make grid[i][j] empty
	this->gridData[this->col * i + j] = '.';
}

char Game::getChar(int i, int j){
	//get char of grid[i][j]
	return this->gridData[i*this->col + j];
}

void Game::setChar(int i, int j, char name){
	//set grid[i][j] as name
	gridData[i*this->col + j] = name;
}

void Game::initializeMoveChecker(){
	//initialize moveChecker after single step execution
	this->moveChecker.clear();
	for (int i=0; i<this->row; i++){
		for (int j=0; j<this->col; j++){
			this->moveChecker[i*this->col + j] = '.';
		}
	}
}

int Game::numberOfSpecies(int i, int j, char name){
	//count the number of name arround grid[i][j] with itself
	int count = 0;		//count of name
	for (int k = i; k < i+3; k++){
		for (int t = j; t < j+3; t++){
			int newK = fixedRow(k-1);
			int newT = fixedCol(t-1);
			if(this->getChar(newK, newT) ==name){
				count++;
			}
		}
	}
	return count;
}

void Game::overpopulation(int i, int j, char name){
	//if grid[i][j] is surronded by more than 3 same species, make it empty
	int count =this->numberOfSpecies(i,j,name) - 1;	//exclude itself
	if (count > 3){
		this->removeCell(i, j);
		//cout<<"overpopulation"<<name<<i<<j<<endl;
	}
}

void Game::predation(int i, int j){
	//If grid[i][j] contains a sheep and has at least one wolf as a neighbor, make it empty.
	int count = this->numberOfSpecies(i,j,'W');
	if (count > 0){
		this->removeCell(i, j);
		//cout<<"killedbywolf"<<i<<j<<endl;
	}
}

void Game::killedByFarmer(int i, int j){
	//if grid[i][j] has at least one neighbor who is a farmer => make it empty.
	int count = this->numberOfSpecies(i, j, 'F');
	if (count >0){
		this->removeCell(i, j);
		//cout<<"killedbyfarmer"<<i<<j<<endl;
	}
}

void Game::starvation(int i, int j){
	//contains a wolf and has only wolf and empty neighbors => make it empty.
	int farmerCount = this->numberOfSpecies(i, j, 'F');
	int sheepCount = this->numberOfSpecies(i, j, 'S');
	if (farmerCount==0 && sheepCount==0){
		this->removeCell(i, j);
		//cout<<"starvation"<<i<<j<<endl;
	}
}

void Game::farmerMove(int i, int j){
	//if farmer contains 1 empty neighbor, move to random empty cell. No empty => stay
	if(this->moveChecker[i*this->col + j] == '.'){
		int count = this->numberOfSpecies(i, j, '.');
		srand (time(NULL));
		int decision;
		if (count > 0){
			decision = rand()%count;
			for (int k = i; k < i+3; k++){
				for (int t = j; t < j+3; t++){
					int newK = fixedRow(k-1);
					int newT = fixedCol(t-1);
					if(this->getChar(newK,newT) =='.'){
						if(decision==0){
							this->setChar(i, j, '.');
							this->setChar(newK,newT, 'F');
							this->moveChecker[(newK)*this->col + (newT)] = 'f';
							//cout<<"farmermoveto"<<i<<j<<endl;
						}
						decision--;
					}
				}
			}
		}
	}
}

void Game::reproduction(int i, int j){
	//If empty cell is surrounded by 2 same neighbors, reproduce to the same species.
	//order: sheep => wolf => farmer.
	if (this->numberOfSpecies(i, j, 'S') == 2){
		this->setChar(i,j,'S');
		//cout<<"reproduction S"<<i<<j<<endl;
	}
	else if (this->numberOfSpecies(i, j, 'W') == 2){
		this->setChar(i,j,'W');
		//cout<<"reproduction W"<<i<<j<<endl;
	}
	else if (this->numberOfSpecies(i, j, 'F') == 2){
		this->setChar(i,j,'F');
		//cout<<"reproduction F"<<i<<j<<endl;
	}
}
