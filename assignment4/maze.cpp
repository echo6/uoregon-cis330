#include "maze.hpp"
#include <iostream>
#include <fstream>
#include <string>
using namespace std;


Maze::Maze(int size){
	this->size = size;
}

// read maze from file, find starting location
void Maze::readFromFile(std::ifstream &f){
	std::streampos cPos = f.tellg();
	std::string theline;
	getline(f, theline);
	//get line and save it to mazeData; maze info
	for (int i=0; i<this->size;i++){
		getline(f, theline);
		this->mazeData += theline.substr(0,size);
	}
	//f.seekg(cPos);

	//set start position and row, col position
	for(int i=0; i<this->size;i++){
		for(int j=0; j<this->size;j++){
			if (this->getValue(i, j) == 'x'){
				this->xStart = i;
				this->row = i;
				this->yStart = j;
				this->col = j;
			}
		}
	}

	/*printing
	cout<<this->xStart<<" "<<this->yStart<<endl;
	for(int i=0;i<this->size;i++){
		for(int j=0;j<this->size;j++){
			cout<<this->getValue(i,j);
		}
		cout<<endl;
	}
	*/
}

// make a single step advancing toward the exit
void Maze::step(){
	Direction current = this->currentDirection;
	int row = this->row;
	int col = this->col;
	this->mazeData[row*this->size + col] = '.';
	//case when previous move was RIGHT
	if (current==RIGHT){
		if(this->isValidMove(row, col+1) && this->getValue(row+1, col)=='@'){
			cout<<"RIGHT"<<endl;
			this->col++;
			this->currentDirection = RIGHT;
		}
		else if(this->isValidMove(row+1, col) && this->getValue(row+1, col-1)=='@'){
			cout<<"DOWN"<<endl;
			this->row++;
			this->currentDirection = DOWN;
		}
		else if(this->isValidMove(row-1, col) && this->getValue(row, col+1)=='@'){
			cout<<"UP"<<endl;
			this->row--;
			this->currentDirection = UP;
		}
		else{
			cout<<"LEFT"<<endl;
			this->col--;
			this->currentDirection = LEFT;
		}
	}
	//case when previous move was UP
	else if(current==UP){
		if(this->isValidMove(row-1, col) && this->getValue(row, col+1)=='@'){
			cout<<"UP"<<endl;
			this->row--;
			this->currentDirection = UP;
		}
		else if(this->isValidMove(row, col+1) && this->getValue(row+1, col+1)=='@'){
			cout<<"RIGHT"<<endl;
			this->col++;
			this->currentDirection = RIGHT;
		}
		else if(this->isValidMove(row, col-1) && this->getValue(row-1, col)=='@'){
			cout<<"LEFT"<<endl;
			this->col--;
			this->currentDirection = LEFT;
		}
		else{
			cout<<"DOWN"<<endl;
			this->row++;
			this->currentDirection = DOWN;
		}
	}
	//case when previous move was LEFT
	else if(current==LEFT){
		if(this->isValidMove(row, col-1) &&this->getValue(row-1, col) =='@'){
			cout<<"LEFT"<<endl;
			this->col--;
			this->currentDirection = LEFT;
		}
		else if(this->isValidMove(row-1, col) &&this->getValue(row-1, col+1) =='@'){
			cout<<"UP"<<endl;
			this->row--;
			this->currentDirection = UP;
		}
		else if(this->isValidMove(row+1, col) &&this->getValue(row, col-1) =='@'){
			cout<<"DOWN"<<endl;
			this->row++;
			this->currentDirection = DOWN;
		}
		else{
			cout<<"RIGHT"<<endl;
			this->col++;
			this->currentDirection = RIGHT;
		}
	}
	//case when previous move was DOWN
	else{
		if(this->isValidMove(row+1, col) &&this->getValue(row, col-1)=='@'){
			cout<<"DOWN"<<endl;
			this->row++;
			this->currentDirection = DOWN;
		}
		else if(this->isValidMove(row, col-1) &&this->getValue(row-1, col-1)=='@'){
			cout<<"LEFT"<<endl;
			this->col--;
			this->currentDirection = LEFT;
		}
		else if(this->isValidMove(row, col+1) &&this->getValue(row+1, col)=='@'){
			cout<<"RIGHT"<<endl;
			this->col++;
			this->currentDirection = RIGHT;
		}
		else{
			cout<<"UP"<<endl;
			this->row--;
			this->currentDirection = UP;
		}
	}
	this->mazeData[this->row*this->size + this->col] ='x';
	this->numberOfMoves++;
}

// return true if the maze exit has been reached, false otherwise
bool Maze::atExit(){
	//check if it is exit and able to exit
	return this->atEdge(this->row, this->col);
}

//set row and col to current position of 'x'
void Maze::getCurrentPosition(int &row, int &col){
	//get current position of 'x'
	for(int i=0; i<this->size;i++){
		for(int j=0; j<this->size;j++){
			if (this->getValue(i, j) == 'x'){
				row = i;
				col = j;
			}
		}
	}
}

bool Maze:: atEdge(int x, int y){
	//check if it is on edge or not
	if (x ==0 || y==0){
		return true;
	}
	else if (x==this->size -1 || y==this->size -1){
		return true;
	}
	else
		return false;
}

char Maze:: getValue(int x, int y){
	//get value of current position
	std::string tmpMaze = this->mazeData;
	return tmpMaze[x*this->size + y];
}

bool Maze::isValidMove(int x, int y){
	//if move out of maze return false
	if (x <0 || y<0){
		return false;
	}
	else if (x >= this->size || y >= this->size){
		return false;
	}
	//only valid move
	if(this->getValue(x,y)=='.'){
		return true;
	}
	else{
		return false;
	}
}
