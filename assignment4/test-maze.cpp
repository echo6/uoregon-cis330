#include <iostream>
#include <fstream>
#include <string>
using namespace std;
#include "maze.hpp"


int main(int argc, const char** argv){
    if (argc !=2){
      cout<<"error; no input file name"<<endl;
      return 1;
    }
    //file object
    std::ifstream ifs (argv[1], std::ifstream::in);
    int numberOfTestCases;
    ifs >> numberOfTestCases;
    int currentRow, currentCol;

    for (int i=0; i<numberOfTestCases; i++){
        int mazeSize;
        ifs >>mazeSize;
        //declare new Maze class
        Maze mymaze(mazeSize);
        //Maze *mymaze = new Maze(mazeSize);
        mymaze.readFromFile(ifs);

        cout<<"ENTER"<<endl;
        do{
	    //step one time
            mymaze.step();
            mymaze.getCurrentPosition(currentRow, currentCol);
        }while(!mymaze.atExit());
        cout<<"EXIT"<<endl<<"***"<<endl;
    }
    ifs.close();

    return 0;

}

//g++ -o vec vec.cpp
//./vec
// g++ test-maze.cpp
//.test.out
